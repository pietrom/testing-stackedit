
I already [wrote ](https://javapeanuts.blogspot.com/2020/12/functions-as-first-class-citizens-shell.html) about adopting a *functional* programming style in Bash scripts. Here I want to explore how to build a minimal, reusable functional toolbox for my bash scripts, avoiding redefinition of base *functional bricks* whenever I need them.



> Written with [StackEdit](https://stackedit.io/).
